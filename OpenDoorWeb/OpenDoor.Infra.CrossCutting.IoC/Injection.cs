﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OpenDoor.Domain.AplicationServices;
using OpenDoor.Domain.Interfaces.ApplicationServices;
using OpenDoor.Domain.Interfaces.Repositories;
using OpenDoor.Infra.Data.Repositories;
using System;

namespace OpenDoor.Infra.CrossCutting.IoC
{
    public static class Injection
    {
        public static IServiceCollection RegisterServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton(configuration);
            services.AddScoped<IRepositoryBase, RepositoryBase>();

            services.AddScoped<IUsuarioPermitidoRepository, UsuarioPermitidoRepository>();
            services.AddScoped<IAcessoAppService, AcessoAppService>();

            return services;
        }
    }
}

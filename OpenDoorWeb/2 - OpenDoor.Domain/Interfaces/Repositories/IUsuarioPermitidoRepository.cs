﻿using OpenDoor.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenDoor.Domain.Interfaces.Repositories
{
    public interface IUsuarioPermitidoRepository : IRepositoryBase
    {
        IEnumerable<UsuarioPermitido> GetAll();
        bool VerificarAutorizacao(string rfid);
        void GravarLogAcesso(string rfid, string motivo);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OpenDoor.Domain.Interfaces.ApplicationServices;

namespace Web.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class OpenDoorAPIController : ControllerBase
    {
        private readonly IAcessoAppService _acessoAppService;

        public OpenDoorAPIController(IAcessoAppService acessoAppService)
        {
            _acessoAppService = acessoAppService;
        }
        
        [HttpGet("{rfid}")]
        public ActionResult<string> Get(string rfid)
        {
            var operationResult = _acessoAppService.VerificarAutorizacao(rfid);
            return new ObjectResult(operationResult);
        }

        [HttpPost("{rfid}/{motivo}")]
        public ActionResult<string> Post(string rfid, string motivo)
        {
            var operationResult = _acessoAppService.EfetuarAcesso(rfid, motivo);
            return new ObjectResult(operationResult);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

﻿using Dapper;
using Microsoft.Extensions.Configuration;
using OpenDoor.Domain.Entities;
using OpenDoor.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace OpenDoor.Infra.Data.Repositories
{
    public class UsuarioPermitidoRepository : RepositoryBase, IUsuarioPermitidoRepository
    {
        public UsuarioPermitidoRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public IEnumerable<UsuarioPermitido> GetAll()
        {
            var cn = GetConnection();
            cn.Open();

            var sql = @"SELECT id_usuario     IdUsuario
                             , nome_usuario   NomeUsuario
                             , uid_cracha     UidCracha
                          FROM usuarios_permitidos";

            var result = cn.Query<UsuarioPermitido>(sql);
            cn.Close();

            return result;
        }

        public bool VerificarAutorizacao(string rfid)
        {
            var cn = GetConnection();

            cn.Open();
            var result = ObterIdUsuarioAutorizado(cn, rfid);
            cn.Close();

            return result != null;
        }

        public void GravarLogAcesso(string rfid, string motivo)
        {
            var cn = GetConnection();

            cn.Open();
            var id = ObterIdUsuarioAutorizado(cn, rfid);
            var sqlInsert = @"INSERT
                                INTO log_acessos
                              VALUES ( @id
                                     , @motivo
                                     , @data)";
            cn.Execute(sqlInsert, new { id, motivo, data = DateTime.Now });
            cn.Close();
        }

        private int? ObterIdUsuarioAutorizado(SqlConnection connection, string rfid)
        {
            var sql = @"SELECT id_usuario
                          FROM usuarios_permitidos
                         WHERE uid_cracha = @rfid";
            var result = connection.Query<int?>(sql, new { rfid }).SingleOrDefault();
            return result;
        }
    }
}

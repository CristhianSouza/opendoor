import { useState, useEffect } from "react";
import { isPlatform } from '@ionic/react';


export function enviarRequisicao() {  

    presentLoading();

    const req = async () => {
     
    };
  
    return {
        req
    };
  }

  async function presentLoading() {
    const loading = document.createElement('ion-loading');
    loading.message = 'Por favor aguarde...';
    loading.duration = 2000;
  
    document.body.appendChild(loading);
    await loading.present();
  
    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }
  
import { IonLoading, IonButtons, IonContent, IonHeader, IonIcon, IonItem, IonList, IonMenuButton, IonPage, IonTitle, IonToolbar, IonButton, IonLabel } from '@ionic/react';
import { americanFootball, basketball, beer, bluetooth, boat, build, flask, football, paperPlane, wifi } from 'ionicons/icons';
import {enviarRequisicao} from '../hooks/Doors';
import React, { useState } from 'react';

const DoorsPage: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Portas</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        <ListItems />
      </IonContent>
    </IonPage>
  );
};

const ListItems = () => {
  const icons = [
    flask,
    wifi,
    beer,
    football,
    basketball,
    paperPlane,
    americanFootball,
    boat,
    bluetooth,
    build
  ];

  let iconsNum = 1;
  const items = ['Laboratório', 'Servidor'].map(x => {
    return (
        <IonItem button onClick={() => enviarRequisicao() } color="default" lines="none">
        <IonIcon icon={icons[iconsNum++ - 1]} slot="start" />  {x}  
        <IonLabel>
        </IonLabel>
      </IonItem>
    );
  });

  return <IonList>{items}</IonList>;
};

export default DoorsPage;

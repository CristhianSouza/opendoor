CREATE TABLE OpenDoor.dbo.log_acessos(
	id_log INT NOT NULL IDENTITY(1,1),
	id_usuario INT NOT NULL,
	motivo_acesso NVARCHAR(254) NOT NULL,
	data_acesso DATETIME NOT NULL,
	CONSTRAINT pk_log_acessos PRIMARY KEY (id_log),
	CONSTRAINT fk_log_acessos_usuarios_permitidos FOREIGN KEY (id_usuario)
		REFERENCES usuarios_permitidos(id_usuario)
);
CREATE TABLE OpenDoor.dbo.motivo_acesso_padrao(
	id_motivo_acesso INT NOT NULL IDENTITY(1,1),
	descricao_motivo NVARCHAR(254) NOT NULL,
	CONSTRAINT pk_motivo_acesso_padrao PRIMARY KEY (id_motivo_acesso),
	CONSTRAINT uk_descricao_motivo UNIQUE (descricao_motivo)
);
CREATE TABLE OpenDoor.dbo.usuarios_permitidos(
	id_usuario INT NOT NULL IDENTITY(1,1),
	uid_cracha NVARCHAR(30) NOT NULL,
	nome_usuario NVARCHAR(254),
	CONSTRAINT pk_usuarios_permitidos PRIMARY KEY (id_usuario)
);
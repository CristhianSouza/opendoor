﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenDoor.Domain.ResultModel
{
    public class OperationResult
    {
        public string Type { get; set; }
        public string Message { get; set; }
    }
}

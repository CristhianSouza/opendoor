﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenDoor.Domain.Entities
{
    public class UsuarioPermitido
    {
        public int IdUsuario { get; set; }
        public string UidCracha { get; set; }
        public string NomeUsuario { get; set; }
    }
}

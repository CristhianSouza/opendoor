﻿using OpenDoor.Domain.Entities;
using OpenDoor.Domain.Interfaces.ApplicationServices;
using OpenDoor.Domain.Interfaces.Repositories;
using OpenDoor.Domain.ResultModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenDoor.Domain.AplicationServices
{
    public class AcessoAppService : IAcessoAppService
    {
        private readonly IUsuarioPermitidoRepository _usuarioPermitidoRepository;

        public AcessoAppService(IUsuarioPermitidoRepository usuarioPermitidoRepository)
        {
            _usuarioPermitidoRepository = usuarioPermitidoRepository;
        }

        public IEnumerable<UsuarioPermitido> ObterTodosUsuariosPermitidos()
        {
            return _usuarioPermitidoRepository.GetAll();
        }

        public OperationResult VerificarAutorizacao(string rfid)
        {
            OperationResult result = new OperationResult();
            try
            {
                var autorizado = _usuarioPermitidoRepository.VerificarAutorizacao(rfid);
                if (autorizado)
                {
                    result.Type = "S";
                    result.Message = "Usuário autorizado.";
                }
                else
                {
                    result.Type = "E";
                    result.Message = "Usuário não autorizado.";
                }
            }
            catch(Exception ex)
            {
                result.Type = "E";
                result.Message = "Erro: " + ex.Message + " Contate o administrador do sistema.";
            }

            return result;
        }

        public OperationResult EfetuarAcesso(string rfid, string motivo)
        {
            OperationResult result = new OperationResult();
            try
            {
                _usuarioPermitidoRepository.GravarLogAcesso(rfid, motivo);
                //Abrir porta
                result.Type = "S";
                result.Message = "Acesso autorizado.";
            }
            catch(Exception ex)
            {
                result.Type = "S";
                result.Message = "Erro: " + ex.Message + " Contate o administrador do sistema.";
            }

            return result;
        }

        public void Dispose()
        {
            _usuarioPermitidoRepository.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}

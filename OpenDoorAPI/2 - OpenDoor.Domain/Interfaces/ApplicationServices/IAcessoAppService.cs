﻿using OpenDoor.Domain.Entities;
using OpenDoor.Domain.ResultModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenDoor.Domain.Interfaces.ApplicationServices
{
    public interface IAcessoAppService : IDisposable
    {
        IEnumerable<UsuarioPermitido> ObterTodosUsuariosPermitidos();
        OperationResult VerificarAutorizacao(string rfid);
        OperationResult EfetuarAcesso(string rfid, string motivo);
    }
}

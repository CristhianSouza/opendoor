﻿using Microsoft.Extensions.Configuration;
using OpenDoor.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace OpenDoor.Infra.Data.Repositories
{
    public class RepositoryBase : IRepositoryBase
    {
        private readonly IConfiguration _configuration;

        public RepositoryBase(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected SqlConnection GetConnection()
        {
            var connectionString = _configuration.GetConnectionString("SqlServer");
            return new SqlConnection(connectionString);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
